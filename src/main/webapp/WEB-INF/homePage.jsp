<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Welcome</title>
</head>
<body>
	<h1>Welcome, <c:out value="${user.email}" /></h1>
	<a href="/logout">Logout</a>
	
	 <h2>Lista Estudiantes</h2>
 
	 <ul>
	 	<li><a href="/estudiantes/crear">Crear estudiantes</a></li>
	 	<li><a href="/contactos/crear">Crear contacto</a></li>
	 	<li><a href="/estudiantes/listar">Lista de Estudiantes</a></li>
	 	<li><a href="/dormitorio/crear">Crear Dormitorio</a></li>
	 	<li><a href="/dormitorio/listar">Listar Dormitorios</a></li>
	 	<li><a href="/curso/crear">Crear Curso</a></li>
	 	<li><a href="/curso/listar">Listar Cursos</a></li>
	 </ul>
	
</body>
</html>